package main

import (
	"github.com/ekomobile/dadata/v2/api/suggest"
	"github.com/ekomobile/dadata/v2/client"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"
)

func TestGeoService_AddressSearch(t *testing.T) {
	mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"suggestions":[{"value":"Test Address","data":{"city":"Test City","street":"Test Street","house":"1","geo_lat":"0.0","geo_lon":"0.0"}}]}`))
	}))
	defer mockServer.Close()

	endpointUrl, _ := url.Parse(mockServer.URL)
	creds := client.Credentials{
		ApiKeyValue:    "test_api_key",
		SecretKeyValue: "test_secret_key",
	}
	api := suggest.Api{
		Client: client.NewClient(endpointUrl, client.WithCredentialProvider(&creds)),
	}
	service := &GeoService{
		api:       &api,
		apiKey:    "test_api_key",
		secretKey: "test_secret_key",
	}

	tests := []struct {
		name    string
		args    string
		want    []*Address
		wantErr bool
	}{
		{
			name: "Valid Address Search",
			args: "Test",
			want: []*Address{
				{City: "Test City", Street: "Test Street", House: "1", Lat: "0.0", Lon: "0.0"},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := service.AddressSearch(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("AddressSearch() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AddressSearch() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewGeoService(t *testing.T) {
	tests := []struct {
		name      string
		apiKey    string
		secretKey string
		want      *GeoService
	}{
		{
			name:      "Create GeoService",
			apiKey:    "test_api_key",
			secretKey: "test_secret_key",
			want: &GeoService{
				apiKey:    "test_api_key",
				secretKey: "test_secret_key",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewGeoService(tt.apiKey, tt.secretKey)
			if got.apiKey != tt.want.apiKey || got.secretKey != tt.want.secretKey {
				t.Errorf("NewGeoService() = %v, want %v", got, tt.want)
			}
		})
	}
}
