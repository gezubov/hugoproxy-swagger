package main

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestNewReverseProxy(t *testing.T) {
	type args struct {
		host string
		port string
	}
	tests := []struct {
		name string
		args args
		want *ReverseProxy
	}{
		{
			name: "Test NewReverseProxy",
			args: args{host: "example.com", port: "8080"},
			want: &ReverseProxy{host: "example.com", port: "8080"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewReverseProxy(tt.args.host, tt.args.port); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewReverseProxy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReverseProxy_ReverseProxy(t *testing.T) {
	type fields struct {
		host string
		port string
	}
	type args struct {
		next http.Handler
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		path   string
		want   int
	}{
		{
			name:   "Test API Route",
			fields: fields{host: "localhost", port: "8080"},
			args:   args{next: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })},
			path:   "/api",
			want:   http.StatusOK,
		},
		{
			name:   "Test Hugo Route",
			fields: fields{host: "localhost", port: "1313"},
			args:   args{next: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })},
			path:   "/",
			want:   http.StatusOK,
		},
		{
			name:   "Test API Route Wrong",
			fields: fields{host: "localhost", port: "8080"},
			args:   args{next: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusNotFound) })},
			path:   "/wrongpath",
			want:   http.StatusNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rp := &ReverseProxy{
				host: tt.fields.host,
				port: tt.fields.port,
			}

			req, err := http.NewRequest("GET", tt.path, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()

			handler := rp.ReverseProxy(tt.args.next)
			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != tt.want {
				t.Errorf("ReverseProxy() = %v, want %v", status, tt.want)
			}
		})
	}
}
