package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
)

type SearchRequest struct {
	Query string `json:"query"`
}

type SearchResponse struct {
	Addresses []*Address `json:"addresses"`
}

// Маршрут: /api/address/geocode метод POST
type GeocodeRequest struct {
	Lat string `json:"lat"`
	Lng string `json:"lng"`
}

type GeocodeResponse struct {
	Addresses []*Address `json:"addresses"`
}

func routes() http.Handler {

	r := chi.NewRouter()
	proxy := NewReverseProxy("hugo", "1313")

	r.Use(proxy.ReverseProxy)
	r.Use(middleware.Logger)

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("doc.json"),
	))

	r.Route("/api", func(r chi.Router) {
		r.Get("/", apiHandler)

		r.Route("/address", func(r chi.Router) {
			r.Post("/geocode", geoHandler)
			r.Post("/search", searchHandler)
		})
	})
	return r
}

//const content = ``
//
//func WorkerTest() {
//	t := time.NewTicker(1 * time.Second)
//	var b byte = 0
//	for {
//		select {
//		case <-t.C:
//			err := os.WriteFile("/app/static/_index.md", []byte(fmt.Sprint(content, b)), 0644)
//			if err != nil {
//				log.Println(err)
//			}
//			b++
//		}
//	}
//}

// ApiHandler godoc
// @Summary apiHandler string
// @Description Just print Hello
//
//	@Tags hello
//	@Accept "/"
//	@Produce plain
//	@Success 200	{string} string	"Hello from API"
//
// @Failure 400 {object} nil "Bad request"
// @Failure 404 {object} nil "Not found"
// @Router /api [get]
func apiHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from API"))
}

// @Summary Address Search
// @Description Search for addresses by query
// @Tags address
// @Accept  json
// @Produce  json
// @Param   query  body  SearchRequest  true  "Search query"
// @Success 200 {object} SearchResponse
// @Failure 400 {string} string "Invalid request format"
// @Failure 500 {string} string "Failed to search address"
// @Router /address/search [post]
func searchHandler(w http.ResponseWriter, r *http.Request) {
	var req SearchRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Invalid request format", http.StatusBadRequest)
		return
	}
	geoService := getGeoService()
	addresses, err := geoService.AddressSearch(req.Query)
	if err != nil {
		http.Error(w, "Failed to search address", http.StatusInternalServerError)
		return
	}

	resp := SearchResponse{Addresses: addresses}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

// @Summary Geocode
// @Description Get addresses by geocoding coordinates
// @Tags address
// @Accept  json
// @Produce  json
// @Param   geocode  body  GeocodeRequest  true  "Geocode coordinates"
// @Success 200 {object} GeocodeResponse
// @Failure 400 {string} string "Invalid request format"
// @Failure 500 {string} string "Failed to geocode address"
// @Router /address/geocode [post]
func geoHandler(w http.ResponseWriter, r *http.Request) {
	var req GeocodeRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Invalid request format", http.StatusBadRequest)
		return
	}
	geoService := getGeoService()
	addresses, err := geoService.GeoCode(req.Lat, req.Lng)
	fmt.Println(addresses)
	if err != nil {
		http.Error(w, "Failed to geocode address", http.StatusInternalServerError)
		return
	}

	resp := GeocodeResponse{Addresses: addresses}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}
