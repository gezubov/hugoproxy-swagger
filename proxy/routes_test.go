package main

import (
	"net/http"
	"reflect"
	"testing"
)

func Test_apiHandler(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			apiHandler(tt.args.w, tt.args.r)
		})
	}
}

func Test_geoHandler(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			geoHandler(tt.args.w, tt.args.r)
		})
	}
}

func Test_routes(t *testing.T) {
	tests := []struct {
		name string
		want http.Handler
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := routes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("routes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_searchHandler(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			searchHandler(tt.args.w, tt.args.r)
		})
	}
}
