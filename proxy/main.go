package main

import (
	"github.com/joho/godotenv"
	"net/http"
	_ "test/docs"
)

// @title GeoService API
// @version 1.0
// @description This is a sample server for a GeoService.
// @host localhost:8080
// @BasePath /api

func main() {
	godotenv.Load()

	r := routes()
	http.ListenAndServe(":8080", r)
}
