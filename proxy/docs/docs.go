// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/address/geocode": {
            "post": {
                "description": "Get addresses by geocoding coordinates",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "address"
                ],
                "summary": "Geocode",
                "parameters": [
                    {
                        "description": "Geocode coordinates",
                        "name": "geocode",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/main.GeocodeRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/main.GeocodeResponse"
                        }
                    },
                    "400": {
                        "description": "Invalid request format",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "500": {
                        "description": "Failed to geocode address",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/address/search": {
            "post": {
                "description": "Search for addresses by query",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "address"
                ],
                "summary": "Address Search",
                "parameters": [
                    {
                        "description": "Search query",
                        "name": "query",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/main.SearchRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/main.SearchResponse"
                        }
                    },
                    "400": {
                        "description": "Invalid request format",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "500": {
                        "description": "Failed to search address",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/api": {
            "get": {
                "description": "Just print Hello",
                "consumes": [
                    "\"/\""
                ],
                "produces": [
                    "text/plain"
                ],
                "tags": [
                    "hello"
                ],
                "summary": "apiHandler string",
                "responses": {
                    "200": {
                        "description": "Hello from API",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad request"
                    },
                    "404": {
                        "description": "Not found"
                    }
                }
            }
        }
    },
    "definitions": {
        "main.Address": {
            "type": "object",
            "properties": {
                "city": {
                    "type": "string"
                },
                "house": {
                    "type": "string"
                },
                "lat": {
                    "type": "string"
                },
                "lon": {
                    "type": "string"
                },
                "street": {
                    "type": "string"
                }
            }
        },
        "main.GeocodeRequest": {
            "type": "object",
            "properties": {
                "lat": {
                    "type": "string"
                },
                "lng": {
                    "type": "string"
                }
            }
        },
        "main.GeocodeResponse": {
            "type": "object",
            "properties": {
                "addresses": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/main.Address"
                    }
                }
            }
        },
        "main.SearchRequest": {
            "type": "object",
            "properties": {
                "query": {
                    "type": "string"
                }
            }
        },
        "main.SearchResponse": {
            "type": "object",
            "properties": {
                "addresses": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/main.Address"
                    }
                }
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "localhost:8080",
	BasePath:         "/api",
	Schemes:          []string{},
	Title:            "GeoService API",
	Description:      "This is a sample server for a GeoService.",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
