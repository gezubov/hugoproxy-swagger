package main

import (
	"reflect"
	"testing"
)

func TestGeoCode_Marshal(t *testing.T) {
	tests := []struct {
		name    string
		fields  GeoCode
		want    []byte
		wantErr bool
	}{

		{
			name:    "Empty GeoCode Marshal",
			fields:  GeoCode{Suggestions: []Suggestion{}},
			want:    []byte(`{"suggestions":[]}`),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &GeoCode{
				Suggestions: tt.fields.Suggestions,
			}
			got, err := r.Marshal()
			if (err != nil) != tt.wantErr {
				t.Errorf("Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Marshal() got = %s, want %s", got, tt.want)
			}
		})
	}
}

func TestUnmarshalGeoCode(t *testing.T) {
	tests := []struct {
		name    string
		args    []byte
		want    GeoCode
		wantErr bool
	}{
		{
			name: "Valid GeoCode Unmarshal",
			args: []byte(`{"suggestions":[{"value":"Test Address","unrestricted_value":"Test Address Full","data":{"city":"Test City","street":"Test Street","house":"1","geo_lat":"0.0","geo_lon":"0.0"}}]}`),
			want: GeoCode{
				Suggestions: []Suggestion{
					{
						Value:             "Test Address",
						UnrestrictedValue: "Test Address Full",
						Data: Data{
							City:   "Test City",
							Street: "Test Street",
							House:  "1",
							GeoLat: "0.0",
							GeoLon: "0.0",
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name:    "Empty GeoCode Unmarshal",
			args:    []byte(`{"suggestions":[]}`),
			want:    GeoCode{Suggestions: []Suggestion{}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnmarshalGeoCode(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalGeoCode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnmarshalGeoCode() got = %v, want %v", got, tt.want)
			}
		})
	}
}
