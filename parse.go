package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: go run main.go <directory>")
		return
	}

	// Получаем директорию из аргументов командной строки
	dir := os.Args[1]
	outputFile := "files_content.txt"

	// Открываем файл для записи
	file, err := os.Create(outputFile)
	if err != nil {
		fmt.Printf("Error creating file: %v\n", err)
		return
	}
	defer file.Close()

	// Рекурсивно обходим директорию
	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Если это файл, читаем его содержимое
		if !info.IsDir() {
			content, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}

			// Записываем путь и содержимое файла в текстовый файл
			_, err = file.WriteString(fmt.Sprintf("Path: %s\n", path))
			if err != nil {
				return err
			}
			_, err = file.WriteString(fmt.Sprintf("Content:\n%s\n", content))
			if err != nil {
				return err
			}
			_, err = file.WriteString("\n---\n")
			if err != nil {
				return err
			}
		}
		return nil
	})

	if err != nil {
		fmt.Printf("Error walking the path %v: %v\n", dir, err)
	}
}
